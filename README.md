# TaylorF2 waveform generation

Standalone TaylorF2 waveform generation in the 3 interferometers followed by a plot of the time domain strains.

Just run:
```bash
python main.py input_cbc_files/Binary2.in
```

You can then create your own binary source by adding a `binary.in` file with your input parameters.

[Explanations about what the parameters correspond to in the file to add]
