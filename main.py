import sys
import numpy as np
import time

from Library import utils
from Library import taylor_f2
from Library import plots

if __name__=='__main__':

    if len(sys.argv) != 2:
        print("Warning : wrong arguments in the command line.")
        print("Please use the format: ")
        print("python main.py input_file")
        sys.exit()

    binary_file_name = sys.argv[1]

    sigpar, array_size = utils.binaryFile_to_sigpar(binary_file_name)

    df_params = utils.sigpar_to_df(sigpar, array_size)
    print(df_params.T)

    delta_f = df_params['delta_f'].values[1]
    n_freq = int(array_size / 2)

    # Generate the waveform strain data in the fourier domain in each interferometer
    start = time.time()
    h_wave_1, h_wave_2, h_wave_3 = taylor_f2.TaylorF2_Waveform_Threedetectors(sigpar, n_freq)
    timing_ms = (time.time() - start) * 1000
    print(f'\nTaylorF2 waveform generation in the fourier domain took: {timing_ms:.1f} ms')

    # Plot them in the time domain
    print('Plotting waveforms in the time domain...')
    sampling_frequency = array_size * delta_f
    plots.plot_time_domain_strains([h_wave_1, h_wave_2, h_wave_3], sampling_frequency)

    sys.exit()
