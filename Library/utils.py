import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np
import pandas as pd

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bns_utility_functions as buf

def binaryFile_to_sigpar(binary_file_name):
    """
    Reads a binary.in file and returns the sigpar array and n
    """

    params = np.loadtxt(binary_file_name)

    (RA, Dec, inc, psi, phic, dist, m1, m2, GMST, f_low, Rmin, m1min, m1max, m2min, m2max, Dmin, Dmax)=params

    sigpar, n = buf.CBC_Parameter_Allocation(m1, m2, Dec, RA, inc, psi, phic, dist, GMST, Rmin, m1min+m2min, f_low)

    return sigpar, n

def sigpar_to_freq_and_indices(sigpar,n_freq):
    """
    When computing waveforms and/or their derivatives, Fourier Transform purposes imposes that the array of frequencies has to start from zero and have a power of 2 length=n_freq. Hence the end frequency is higher than f_high.
    We then set to zero all the waveform values corresponding to frequencies f not respecting : f_low <= f <= f_high; where f_low is an input of the binary.in file and f_high calculated from it.


    This function returns:
    return freqs, i_low, i_high

    Where:
        - freqs [array of floats]:
            array of all the frequencies from 0 to n_freq*(delta_f-1) included
        - i_low [int]:
            s.t. freqs[i_low] is the 1st frequency where the waveform is non-zero
        - i_high [int]:
            s.t. freqs[i_high] is the last frequency where the waveform is non-zero
    """

    delta_f = sigpar[17]
    f_low = sigpar[13]
    f_high = sigpar[14]

    freqs = np.arange(0, n_freq*delta_f, delta_f)

    i_low=int(f_low/delta_f)+1
    i_high=int(f_high/delta_f)

    return freqs, i_low, i_high

def sigpar_to_df(sigpar, array_size):
    binaryParams_keys=["mass_1","mass_2","total_mass", "symmetric_mass_ratio", "chirp_mass", "reduced_mass", "inclination", "polarisation", "tc", "phase_gw", "duration", "lat", "long", "luminosity_distance", "gmst", "f_low", "f_high", "min_orb_sep", "delta_f", "array_size"]

    binaryParams_names=["Mass of star 1","Mass of star 2","Total mass", "Symmetric mass ratio", "Chirp mass", "Reduced mass", "Inclination", "Polarisation", "Coalescence time", "Phase at coalescence", "Observation time", "Latitude", "Longitude", "Distance", "GMST", "Signal min freq", "Signal max freq", "Minimum orbital separation R", "DeltaF", "Data array size"]

    binaryParams_units=["solar masses","solar masses","solar masses", "", "solar masses", "solar masses", "radians", "radians", "secs", "radians", "secs", "radians", "radians", "Mpc", "secs", "Hz", "Hz", "", "Hz", ""]

    binaryParams_values=['' for i in range(len(binaryParams_names))]
    binaryParams_values[0]=sigpar[9]
    binaryParams_values[1]=sigpar[10]
    binaryParams_values[2]=sigpar[11]
    binaryParams_values[3]=sigpar[12]
    binaryParams_values[4]=sigpar[4]
    binaryParams_values[5]=sigpar[5]
    binaryParams_values[6]=sigpar[0]
    binaryParams_values[7]=sigpar[2]
    binaryParams_values[8]=sigpar[8]
    binaryParams_values[9]=sigpar[1]
    binaryParams_values[10]=sigpar[16]
    binaryParams_values[11]=sigpar[6]
    binaryParams_values[12]=sigpar[7]
    binaryParams_values[13]=sigpar[3]/MPC
    binaryParams_values[14]=sigpar[18]
    binaryParams_values[15]=sigpar[13]
    binaryParams_values[16]=sigpar[14]
    binaryParams_values[17]=sigpar[19]
    binaryParams_values[18]=sigpar[17]
    binaryParams_values[19]=array_size

    # dic={"Name": binaryParams_names, "Value": binaryParams_values, "Unit": binaryParams_units}
    # df=pd.DataFrame(dic)

    df = pd.DataFrame([binaryParams_names, binaryParams_values, binaryParams_units], columns=binaryParams_keys, index=['Name', 'Value', 'Unit'])

    return df
