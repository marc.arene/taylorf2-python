from numpy import cos, sin


def LVC_Detector_DeltaGMST(RA, Dec, GMST, detectors):
    """
    Time off set :   Livingston - Hanford, Virgo - Handford.
    Inputs:
    -------
        double RA
        double Dec
        double GMST
        int detectors
    Results:
    --------
        double *DeltaGMST
    """
  ## Hanford is taken as a reference to compute the time delay
    if detectors==1:      ## Time delay      Livingston - Hanford
        return -0.006957129568*cos(Dec)*cos(GMST - RA) - 0.00553862846*cos(Dec)*sin(GMST - RA) + 0.00458697738*sin(Dec)

    elif detectors==2:    ## Time delay        Virgo - Hanford
        return -0.0223748*cos(Dec)*cos(GMST - RA) + 0.0156031*cos(Dec)*sin(GMST - RA) + 0.000739756 *sin(Dec)

    else:
        my_logger.error("You have made an invalid choice of detectors.  Please try 1 for (L - H), 2 for (V - H)\n")

def LVC_Detector_Beam_Pattern_Functions(RA,  Dec,  psi,  GMST, detector):
    """
    Compute Fplus and Fcross for Hanford (1), Livingston (2) and Virgo (3).
    Inputs:
    -------
        double *Fplus
        double *Fcross
        double RA
        double Dec
        double psi
        double GMST
        int detector
    Results:
    --------
        return (Fplus, Fcross)
    """

    cD = cos(Dec)
    sD = sin(Dec)
    cD2 = cD * cD
    sD2 = sD * sD
    c2D = cos(2.0 * Dec)
    s2D = sin(2.0 * Dec)
    cp = cos(psi)
    sp = sin(psi)
    cp2 = cp * cp
    sp2 = sp * sp
    c2p = cos(2.0 * psi)
    s2p = sin(2.0 * psi)
    x = GMST - RA
    cx = cos(x)
    sx = sin(x)
    cx2 = cx * cx
    sx2 = sx * sx
    c2x = cos(2.0 * x)
    s2x = sin(2.0 * x)



    if detector==1 : ## LIGO Hanford

        Fplus = - 0.0730900332031063 * cD2 * c2p - 0.3926141000568871 * cp2 * sx2 + ( 0.03880670618274684 * c2D - 0.1164201185482405 ) * c2p * s2x - 0.3195240668537807 * c2p * sx2 * sD2 + cx2 * c2p * ( 0.3195240668537807 + 0.3926141000568871 * sD2 ) + 0.3926141000568871 * sx2 * sp2 + cD * sx * ( 0.455995666157894 * sD * (sp2 - cp2) + 0.989556178693002 * cp * sp ) + cx * cD * ( 0.4947780893465008 * sD * (sp2 - cp2) - 0.911991332315788 * cp * sp ) - sD * s2p * ( 0.1552268247309873 * c2x + 0.7121381669106678 * s2x )
        Fcross = 0.4947780893465008 * cD * c2p * sx - 0.1552268247309874 * cx2 * c2p * sD + cp2 * sD * ( 0.1552268247309874 * sx2 - 0.7121381669106678 * s2x ) + cp * sp * ( 0.1096350498046595 + c2x * ( 0.356069083455334 * c2D - 1.068207250366002 ) +  0.1096350498046595 * c2D + 0.455995666157894 * sx * s2D ) + ( 0.7121381669106678 * s2x - 0.1552268247309874 * sx2 ) * sD * sp2 + cx * cD * ( 0.989556178693002 * cp * sD * sp - 0.455995666157894 * c2p ) + s2x * s2p * ( 0.07761341236549368 * (1.0 + sD2) )
        return Fplus, Fcross

    elif detector==2 : ## LIGO Livingston

        Fplus =   0.3022751652944559 * cD2 * c2p + 0.4112808558135931 * cp2 * sx2 + ( 0.2103153986100414 - 0.0701051328700138 * c2D) * c2p * s2x + 0.1090056905191372 * c2p * sx2 * sD2 + cx2 * c2p * ( - 0.1090056905191372 - 0.4112808558135931 * sD2 ) - 0.4112808558135931 * sx2 * sp2 + cx * cD * ( 0.494589182862555 * sD * (cp2 - sp2) + 0.7264625214380846 * cp * sp ) + sx * cD * ( 0.3632312607190423 * sD * (cp2 - sp2) - 0.98917836572511 * cp * sp ) + sD * s2p * ( 0.2804205314800552 * c2x + 0.5202865463327303 * s2x )
        Fcross = - 0.494589182862555 * cD * c2p * sx + 0.2804205314800552 * cx2 * c2p * sD + cp2 * sD * ( - 0.2804205314800552 * sx2 + 0.5202865463327303 * s2x ) + cp * sp * ( - 0.4534127479416838 + c2x * ( 0.7804298194990955 - 0.2601432731663651 * c2D ) - 0.4534127479416838 * c2D - 0.3632312607190423 * sx * s2D ) + ( 0.2804205314800552 * sx2 - 0.5202865463327303 * s2x ) * sD * sp2 + cx * cD * ( 0.3632312607190423 * c2p - 0.98917836572511 * cp * sD * sp ) + s2x * s2p * ( - 0.1402102657400276 * (1.0 + sD2))
        return Fplus, Fcross

    elif detector==3 : ## Virgo

        Fplus = -0.2039518040190771  * cD2 * c2p + 0.24387403591736 * cp2 * sx2 + ( -0.148625668755911 + 0.04954188958530365  * c2D) * c2p * s2x + 0.4478258399364372 * c2p * sx2 * sD2 + cx2 * c2p * ( - 0.4478258399364372 - 0.24387403591736 * sD2 ) - 0.24387403591736 * sx2 * sp2 +  cD * sx * ( 0.3756662065403988 * sD * (sp2 - cp2) + 0.930304867751558 * cp * sp ) + cx * cD * ( 0.4651524338757792 * sD * (sp2 - cp2) - 0.7513324130807977 * cp * sp ) + sD * s2p * ( -0.1981675583412146 * c2x + 0.6916998758537972 * s2x )
        Fcross = 0.4651524338757792 * cD * c2p * sx - 0.1981675583412146 * cx2 * c2p * sD + cp2 * sD * ( 0.1981675583412146 * sx2 + 0.6916998758537972 * s2x ) + cp * sp * ( 0.3059277060286157 + c2x * ( 1.037549813780696 - 0.3458499379268986 * c2D ) + 0.3059277060286157  * c2D + 0.3756662065403989 * sx * s2D ) + ( -0.1981675583412146 * sx2 - 0.6916998758537972 * s2x ) * sD * sp2 + cx * cD * ( -0.3756662065403988 * c2p + 0.930304867751558 * cp * sD * sp ) + s2x * s2p * ( 0.0990837791706073 * (1.0 + sD2))
        return Fplus, Fcross
    else:
        my_logger.error("You have made an invalid choice of detector.  Please try 1 for LHO, 2 for LLO and 3 for Virgo\n")
