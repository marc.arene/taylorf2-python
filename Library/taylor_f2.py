import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')
import numpy as np

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import Library.bns_utility_functions as buf
import Library.lvc_detector_functions as lvdf
import Library.utils as utils

def ZipMerge(h_real,h_img):
    """
    Returns the one dimensional array 'h' such that h_real elements take position on
    the even indices of h and h_img take place on the odd indices.

    Inputs:
    -------
        h_real: list
        h_img: list
    Output:
    -------
        h: list

    Example:
    --------
        >> h_real=[0,2,4,6]
        >> h_img=[1,3,5,7]
        >> h = ZipMerge(h_real,h_img)
        >> h
        [0, 1, 2, 3, 4, 5, 6, 7]

    Notes:
        - if numpy arrays are used for inputs, the output h will still be a 'classical' list
        cf https://stackoverflow.com/questions/10632839/python-transform-list-of-tuples-in-to-1-flat-list-or-1-matrix
    """
    h_real = h_real[:, np.newaxis]
    h_img = h_img[:, np.newaxis]

    return np.hstack((h_real,h_img)).flatten()

def Taylorf2_phase_3_5PN(f, psi, m_secs, eta, tc, phic):
    """
    Compute the phase defined by the TaylorF2 model at 3.5 PN order.
    Inputs:
    -------
        double f
        double *psi
        double m_secs
        double eta
        double tc
        double phic
    Results:
    --------
        double *phase
    """

    v = pow((PI * m_secs * f), (1.0/3.0))
    v5 = v*v*v*v*v
    tpftc = TPI * f * tc
    return tpftc - phic - PIb4 + ((psi[0] / v5) * (1.0 + v * (v * (psi[1] + v * (psi[2] + v * (psi[3] + v * ((psi[4] * (1.0 + (3.0 * np.log(v)))) + v * ((psi[5] + Tf2_12 * np.log(4.0 * v)) + v * psi[6]))))))))

def TaylorF2_Waveform_Threedetectors_old(sigpar, n_freq):
    """
    3-detectors version of TaylorF2 waveform computation.
    Inputs:
    -------
        double *sigpar
        int n_freq
    Results:
    --------
        return h_wave1, h_wave2, h_wave3
    """
    psi = [None for i in range(7)]

    # h_wave1=np.zeros(2*n_freq)
    # h_wave2=np.zeros(2*n_freq)
    # h_wave3=np.zeros(2*n_freq)

    # related quantities
    Mt_sec=sigpar[11]*GEOM           # Total mass in secs
    Mc_sec = sigpar[4] * GEOM        # Chirp mass in secs
    c76 = -7.0/6.0
    ci = np.cos(sigpar[0])

    # Refer to equation 5.24 in Yann's thesis to understand the following calculations

    # Hanford
    F_plus_H, F_cross_H = lvdf.LVC_Detector_Beam_Pattern_Functions(sigpar[7], sigpar[6], sigpar[2], sigpar[18],1)
    A_detector_H =  0.5*(1.0 + ci**2)*F_plus_H
    B_detector_H =  ci*F_cross_H
    C_detector_H = np.sqrt(A_detector_H*A_detector_H+B_detector_H*B_detector_H)

    # Livingston
    delta_LH = lvdf.LVC_Detector_DeltaGMST(sigpar[7], sigpar[6], sigpar[18],1)
    F_plus_L, F_cross_L = lvdf.LVC_Detector_Beam_Pattern_Functions(sigpar[7], sigpar[6], sigpar[2], delta_LH*(TPI/86400.0),2)
    A_detector_L =  0.5*(1.0 + ci**2)*F_plus_L
    B_detector_L =  ci*F_cross_L
    C_detector_L = np.sqrt(A_detector_L*A_detector_L+B_detector_L*B_detector_L)

    # Virgo
    delta_VH = lvdf.LVC_Detector_DeltaGMST(sigpar[7], sigpar[6], sigpar[18],2)
    F_plus_V, F_cross_V = lvdf.LVC_Detector_Beam_Pattern_Functions(sigpar[7], sigpar[6], sigpar[2], delta_VH*(TPI/86400.0),3)
    A_detector_V =  0.5*(1.0 + ci**2)*F_plus_V
    B_detector_V =  ci*F_cross_V
    C_detector_V = np.sqrt(A_detector_V*A_detector_V+B_detector_V*B_detector_V)


    A0 = (SOL/(sigpar[3]*pow(PI,2.0/3.0)))*(np.sqrt(5.0/24.0))*pow(Mc_sec,5.0/6.0)
    A_H = A0*C_detector_H
    A_L = A0*C_detector_L
    A_V = A0*C_detector_V

    eta = sigpar[12]
    eta2 = eta * eta
    eta3 = eta2 * eta
    psi[0] = Tf2_1 / eta
    psi[1] = Tf2_2 + (Tf2_3 * eta)
    psi[2] = Tf2_4 * PI
    psi[3] = Tf2_5 + (Tf2_6 * eta) + (Tf2_7 * eta2)
    psi[4] = (Tf2_8 + (Tf2_9 * eta)) * PI
    psi[5] = Tf2_10 + (Tf2_11 * PI2) + (Tf2_12 * EULER) + (((Tf2_13 * PI2) + Tf2_14) * eta) + (Tf2_15 * eta2) + (Tf2_16 * eta3)
    psi[6] = PI * (Tf2_17 + (Tf2_18 * eta) + (Tf2_19 * eta2))

    deltaT_TPI_LH = TPI*delta_LH
    deltaT_TPI_VH = TPI*delta_VH


    # print("Entering the new python code of TaylorF2 !")
    # Replacing the loop over the range of frequency with np.array manipulations

    # Retrieve the frequencies f s.t. : f_low <= f <= f_high, for which we are not going to set the waveform values to zero
    all_freqs, i_low_num, i_high_num = utils.sigpar_to_freq_and_indices(sigpar, n_freq)
    freq = all_freqs[i_low_num:i_high_num+1]

    AmpWave = freq**c76
    phi_wave = Taylorf2_phase_3_5PN(freq, psi, Mt_sec, sigpar[12], sigpar[8], sigpar[1])

    h_wave1_real = A_H * AmpWave*np.cos(phi_wave)
    h_wave1_img = A_H * AmpWave*np.sin(phi_wave)
    h_wave1 = ZipMerge(h_wave1_real,h_wave1_img)
    # h_wave1 = [h_wave1_real[i//2] if i%2==0 else h_wave1_img[i//2] for i in range(2*len(freq))]

    c0 = phi_wave - deltaT_TPI_LH*freq
    h_wave2_real = A_L * AmpWave*np.cos(c0)
    h_wave2_img = A_L * AmpWave*np.sin(c0)
    h_wave2 = ZipMerge(h_wave2_real,h_wave2_img)

    c1 = phi_wave - deltaT_TPI_VH*freq
    h_wave3_real = A_V * AmpWave*np.cos(c1)
    h_wave3_img = A_V * AmpWave*np.sin(c1)
    h_wave3 = ZipMerge(h_wave3_real,h_wave3_img)

    # Now we should pad the three waveforms with zeros for frequencies below f_low and above f_high
    # Considering we would only pad the real part of h_wave1=h_wave1_real:
    # After padding on the left, h_wave1_real[i_low_num] will be the (i_low_num+1)th frequency evaluation of h_wave1_real. Hence we need to pad with exactly i_low_num zeros
    # After padding on the right, h_wave1_real[i_high_num] will be the (i_high_num+1)th frequency evaluation of h_wave1_real. But we want the final length len(h_wave1_real) to be equal to n_freq. Hence we must pad with n_freq-(i_high_num+1) on the right.

    n1=2*(i_low_num)
    n2=2*(n_freq-(i_high_num+1))

    if len(h_wave1)+n1+n2 != 2*n_freq:
        raise ValueError("Error: the waveform lengths are {} when it should be exactly twice n_freq, ie {}".format(len(h_wave1),2*n_freq))

    # Pad with zeros:
    h_wave1=np.pad(h_wave1,(n1,n2),'constant',constant_values=(0,0))
    h_wave2=np.pad(h_wave2,(n1,n2),'constant',constant_values=(0,0))
    h_wave3=np.pad(h_wave3,(n1,n2),'constant',constant_values=(0,0))

    return h_wave1, h_wave2, h_wave3

def TaylorF2_Waveform_Threedetectors(sigpar, n_freq):
    """
    3-detectors version of TaylorF2 waveform computation.
    Inputs:
    -------
        double *sigpar
        int n_freq
    Results:
    --------
        return h_wave1, h_wave2, h_wave3
    """
    psi = [None for i in range(7)]

    # related quantities
    Mt_sec=sigpar[11]*GEOM           # Total mass in secs
    Mc_sec = sigpar[4] * GEOM        # Chirp mass in secs
    c76 = -7.0/6.0
    ci = np.cos(sigpar[0])

    # Refer to equation 5.24 in Yann's thesis to understand the following calculations

    # Hanford
    F_plus_H, F_cross_H = lvdf.LVC_Detector_Beam_Pattern_Functions(sigpar[7], sigpar[6], sigpar[2], sigpar[18],1)
    A_detector_H =  0.5*(1.0 + ci**2)*F_plus_H
    B_detector_H =  ci*F_cross_H
    C_detector_H = np.sqrt(A_detector_H*A_detector_H+B_detector_H*B_detector_H)

    # Livingston
    delta_LH = lvdf.LVC_Detector_DeltaGMST(sigpar[7], sigpar[6], sigpar[18],1)
    F_plus_L, F_cross_L = lvdf.LVC_Detector_Beam_Pattern_Functions(sigpar[7], sigpar[6], sigpar[2], delta_LH*(TPI/86400.0),2)
    A_detector_L =  0.5*(1.0 + ci**2)*F_plus_L
    B_detector_L =  ci*F_cross_L
    C_detector_L = np.sqrt(A_detector_L*A_detector_L+B_detector_L*B_detector_L)

    # Virgo
    delta_VH = lvdf.LVC_Detector_DeltaGMST(sigpar[7], sigpar[6], sigpar[18],2)
    F_plus_V, F_cross_V = lvdf.LVC_Detector_Beam_Pattern_Functions(sigpar[7], sigpar[6], sigpar[2], delta_VH*(TPI/86400.0),3)
    A_detector_V =  0.5*(1.0 + ci**2)*F_plus_V
    B_detector_V =  ci*F_cross_V
    C_detector_V = np.sqrt(A_detector_V*A_detector_V+B_detector_V*B_detector_V)



    A0 = (SOL/(sigpar[3]*pow(PI,2.0/3.0)))*(np.sqrt(5.0/24.0))*pow(Mc_sec,5.0/6.0)
    A_H = A0*C_detector_H
    A_L = A0*C_detector_L
    A_V = A0*C_detector_V

    eta = sigpar[12]
    eta2 = eta * eta
    eta3 = eta2 * eta
    psi[0] = Tf2_1 / eta
    psi[1] = Tf2_2 + (Tf2_3 * eta)
    psi[2] = Tf2_4 * PI
    psi[3] = Tf2_5 + (Tf2_6 * eta) + (Tf2_7 * eta2)
    psi[4] = (Tf2_8 + (Tf2_9 * eta)) * PI
    psi[5] = Tf2_10 + (Tf2_11 * PI2) + (Tf2_12 * EULER) + (((Tf2_13 * PI2) + Tf2_14) * eta) + (Tf2_15 * eta2) + (Tf2_16 * eta3)
    psi[6] = PI * (Tf2_17 + (Tf2_18 * eta) + (Tf2_19 * eta2))

    deltaT_TPI_LH = TPI*delta_LH
    deltaT_TPI_VH = TPI*delta_VH

    # Retrieve the frequencies f s.t. : f_low <= f <= f_high, for which we are not going to set the waveform values to zero
    all_freqs, i_low_num, i_high_num = utils.sigpar_to_freq_and_indices(sigpar, n_freq)
    freq = all_freqs[i_low_num:i_high_num+1]

    AmpWave = freq**c76
    phi_wave = Taylorf2_phase_3_5PN(freq, psi, Mt_sec, sigpar[12], sigpar[8], sigpar[1])

    h_wave1 = A_H * AmpWave*np.exp(-1j*phi_wave)

    c0 = phi_wave + deltaT_TPI_LH*freq
    # c0 = phi_wave
    h_wave2 = A_L * AmpWave*np.exp(-1j*c0)

    c1 = phi_wave + deltaT_TPI_VH*freq
    # c1 = phi_wave
    h_wave3 = A_V * AmpWave*np.exp(-1j*c1)
    # h_source = A0 * np.exp(-1j*phi_wave)

    # Now we should pad the three waveforms with zeros for frequencies below f_low and above f_high
    # Considering we would only pad the real part of h_wave1=h_wave1_real:
    # After padding on the left, h_wave1_real[i_low_num] will be the (i_low_num+1)th frequency evaluation of h_wave1_real. Hence we need to pad with exactly i_low_num zeros
    # After padding on the right, h_wave1_real[i_high_num] will be the (i_high_num+1)th frequency evaluation of h_wave1_real. But we want the final length len(h_wave1_real) to be equal to n_freq. Hence we must pad with n_freq-(i_high_num+1) on the right.

    n1=(i_low_num)
    n2=(n_freq-(i_high_num+1))

    # if len(h_wave1)+n1+n2 != 2*n_freq:
    #     raise ValueError("Error: the waveform lengths are {} when it should be exactly twice n_freq, ie {}".format(len(h_wave1),2*n_freq))

    # Pad with zeros:
    h_wave1=np.pad(h_wave1,(n1,n2),'constant',constant_values=(0,0))
    h_wave2=np.pad(h_wave2,(n1,n2),'constant',constant_values=(0,0))
    h_wave3=np.pad(h_wave3,(n1,n2),'constant',constant_values=(0,0))

    return h_wave1, h_wave2, h_wave3
