import sys
# cf https://docs.python.org/3/tutorial/modules.html
sys.path.append('../')

from Headers.PN_Coefficients import * # Headers.Constants already imported in PN_Coeff
import numpy as np


def ComputeChirpTime3p5PN(f_low, sigpar):
    """
    Computes chirp times at 3.5 PN.

    Inputs:
    -------
        f_low: [float] low frequency cutoff of the detector
        sigpar: [array_like] parameters of the system
    Outputs:
    --------

    """
    etaM = sigpar[12]
    etaM2 = etaM*etaM
    Mt_sec = sigpar[11] * GEOM           # Total mass in s

    Tau_0 = tf1 * Mt_sec / etaM
    Tau_2 = tf2 + tf3 * etaM
    Tau_3 = tf4 * PI
    Tau_4 = tf5 + tf6 * etaM + tf7 * etaM2
    Tau_5 = PI*(tf9 + tf8*etaM)
    Tau_6 = tf11 + tf12*PI2 + tf10*(EULER + np.log(4.0)) + ( tf13 + tf14*PI2 )*etaM + tf15*etaM2 + tf16*etaM2*etaM
    Tau_6_log = tf10
    Tau_7 = PI*(tf19 + tf18*etaM + tf17*etaM2 )
    v_0 = pow(PI*Mt_sec*f_low,1.0/3.0)
    v_0_2 = v_0*v_0
    v_0_3 = v_0_2*v_0
    v_0_4 = v_0_3*v_0
    v_0_5 = v_0_4*v_0
    v_0_6 = v_0_5*v_0
    v_0_7 = v_0_6*v_0

    Tau_chirp = Tau_0*pow(v_0,-8.0)*(1.0 + Tau_2*v_0_2 + Tau_3*v_0_3 + Tau_4 *v_0_4 +  Tau_5 *v_0_5 +  (Tau_6 + Tau_6_log*np.log(v_0))*v_0_6 +  Tau_7 *v_0_7 )
    return Tau_chirp

def array_length_calculator(T, samprate):
    """
    Computes the length of the table adapted to the sampling and FFT routines, ie the next power of 2
    greater than the actual length of the table.

    Inputs:
    -------
        T --[float]: time duration of the signal analysed (in sec)
        samprate --[float]: sampling rate (in sec^-1)
        n --[list of int]: the variable to which the value of the new table length will be assigned to. Note that as
                           the type 'int' is immutable, we use the type 'list' in order to be able to do the assignement.
    Result:
    -------
        'n' takes the value of the next power of 2 greater or equal than the actual length of the table (T*samprate)
    """
    N = T * samprate
    NN = np.log10(N) / np.log10(2.0)
    x = pow(2.0 , (int(NN) + 1.0))
    m = int(x)
    return m

def array_length_calculator_bit(T, samprate):
    """
    Computes the length of the table adapted to the sampling and FFT routines, ie the next power of 2
    greater than the actual length of the table.

    Inputs:
    -------
        T --[float]: time duration of the signal analysed (in sec)
        samprate --[float]: sampling rate (in sec^-1)
        n --[list of int]: the variable to which the value of the new table length will be assigned to. Note that as
                           the type 'int' is immutable, we use the type 'list' in order to be able to do the assignement.
    Result:
    -------
        'n' takes the value of the next power of 2 greater or equal than the actual length of the table (T*samprate)
    """
    N = T * samprate
    nextPowerOfTwo=N
    nextPowerOfTwo -=1
    nextPowerOfTwo |= nextPowerOfTwo >> 1
    nextPowerOfTwo |= nextPowerOfTwo >> 2
    nextPowerOfTwo |= nextPowerOfTwo >> 4
    nextPowerOfTwo |= nextPowerOfTwo >> 8
    nextPowerOfTwo |= nextPowerOfTwo >> 16
    nextPowerOfTwo +=1
    return nextPowerOfTwo

def CBC_Parameter_Allocation(m1, m2, dec, RA, inc, psi, phic, DL, GMST, Rmin, mmin, flow, printOutputs=False):
    """
    Allocate the parameters of the BNS.
    Inputs:
    -------
    ...
    Returns:
    --------
        return params, n
            params = array of length 20
            n = length of the sample array (should be a power of 2)
    """

    vmax = np.sqrt(1.0 / Rmin)
    inc *= PI/180.0                                # inclination of the binary (rad)
    psi *= TPI/360                                 # polarisation of the GW (rad)
    phic *= TPI/360                                # phase at the coalescence (rad)
    theta = (PI * dec / 180.0)                     # latitude of the binary (rad). Might actually be the altitude
    phi = TPI * RA / 360.0                         # longitude of the binary (rad). Might actually be the azimuth

    params = [None for i in range(20)]

    params[0] = inc
    params[1] = phic
    params[2] = psi
    params[3] = DL * MPC
    params[9] = m1
    params[10] = m2
    params[11] = m1 + m2                               # m
    params[12] = m1 * m2 / params[11]**2               # eta
    params[4] = params[11] * pow(params[12], 0.6)      # Mc
    params[5] = params[11] * params[12]                # mu
    params[6] = theta
    params[7] = phi
    params[13] = flow
    params[14] = vmax**3 / (PI * GEOM * params[11])      # fmax signal
    params[15] = vmax**3 / (PI * GEOM * mmin)            # fmax integration based on m_min solar masses
    fsamp = 2.0 * params[15]                                # fsamp = 2 fmax_integration

    tchirp = ComputeChirpTime3p5PN(flow, params)
    params[8] = tchirp          # tc
    # Tobs = tchirp
    Tobs = tchirp + 2
    # Tobs = 1.1 * tchirp
    params[16] = Tobs           # Tobs

    n=array_length_calculator(Tobs, fsamp)

    params[17] = 1.0 / Tobs      # df
    params[18] = GMST
    params[19] = Rmin

    #for(i = 0 i < 20 i++) fprintf(stderr, "p[%d] = %lf\n", i, params[i])
    if printOutputs:
        for i in range(len(params)):
            print("p[{}]= {}".format(i,params[i]))

        # print("n= ",n)
        # print("tchirp= ",tchirp)

    return params, n

def LVC_RiemannSum(CA1, CA2, shf, Tobs, n, flow, fhigh):
    """
    Routine used to perform the sum in the Fourier domain used for LogL and SNR.
    Inputs:
    -------
        CA1 [list of floats]: tbc
        CA2 [list of floats]: tbc
        shf [list of floats]: tbc
        Tobs [float]: tbc
        n [int]: tbc
        flow [float]: tbc
        fhigh [float]: tbc
    Returns:
    --------
        [list of floats]= the integration of ...
    """

    df = 1.0/Tobs
    nb2 = int(n/2)
    prod = 0.0
    arg = 0.0
    imin = int(round(flow/df))
    imax = int(round(fhigh/df))

    # ####################################################################### #
    #                   Calculate integrand, i.e. [H(f)G#(f)+H#(f)G(f)]/2     #
    # ####################################################################### #

    if imax > nb2-1:
        imax = nb2-1

    CA1_r = np.array(CA1[2*imin:2*imax+1:2])
    CA1_i = np.array(CA1[2*imin+1:2*imax+2:2])
    CA2_r = np.array(CA2[2*imin:2*imax+1:2])
    CA2_i = np.array(CA2[2*imin+1:2*imax+2:2])

    shf = np.array(shf[imin:imax+1])

    prod = (CA1_r * CA2_r + CA1_i * CA2_i) / shf
    arg = df * prod.sum()

    ## old version using a for loop...
    # for i in range(imin,imax+1):
    #     j = (2 * i)
    #     k = j + 1
    #     prod = ((CA1[j] * CA2[j]) + (CA1[k] * CA2[k])) / (shf[i])
    #     arg += prod
    #
    # arg *= df

    return arg
