import matplotlib.pyplot as plt
import numpy as np
from scipy import signal

def plot_time_domain_strains(strain_fd_array, sampling_frequency):

    plt.figure(figsize=(15,5))

    ifo_names = ['Handford', 'Livingston', 'Virgo']

    for i, strain_fd in enumerate(strain_fd_array):
        dwindow = signal.tukey(strain_fd.size, alpha=1./8)
        strain_td = np.fft.irfft(strain_fd * dwindow)
        number_of_samples = len(strain_td)
        duration_td = number_of_samples / sampling_frequency
        time = np.linspace(start=0, stop=duration_td, num=number_of_samples)
        plt.plot(time, strain_td, linewidth=1, label=ifo_names[i])
        # plt.plot(strain_td, linewidth=1, label=ifo_names[i])

    plt.xlabel("Time (sec)")
    plt.ylabel("strain")
    plt.legend()
    plt.title('Time domain strain data in each interferometer from the TaylorF2 waveform model')
    plt.show()
